% Author: Kaijun Feng
% Last edit: 2/9/2016
% Note: export the iso-frequency curve
function ExportF(expname,handles)
global k_x k_z
dlmwrite(expname,[real(k_x)' real(k_z)']);
end