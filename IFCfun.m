% Author: Kaijun Feng
% Last edit: 2/9/2016
% Note: calculate iso-frequency contour for given materials and frequency
function IFCfun(handles)
%% set up
global k_max k_x k_z
% k_max = 2e4; % 2pi/cm
N = 500;
c = 3e10; % cm/s
omega = str2double(get(handles.edit3,'String'));
%% permittivity
funs = {'eps_InGaAs','eps_AlInAs'};
eps1 = zeros(1,N);
eps2 = zeros(1,N);
n1 = get(handles.slider1,'Value');%cm^-3
n2 = get(handles.slider2,'Value');
identifier1 = get(handles.pop1, 'Value');
identifier2 = get(handles.pop2, 'Value');
eps1 = feval(funs{identifier1},omega,n1);
eps2 = feval(funs{identifier2},omega,n2);
eps_x = (eps1 + eps2)/2;
eps_z = 2 * eps1 * eps2 /(eps1 + eps2);
% Only taking into account the real part of permittivity, because in
% reality k is complex, which is hard to initialize.
eps_x=real(eps_x);
eps_z=real(eps_z);
if real(eps_x) > 0
    k_x = linspace(-k_max,k_max,N);
    k_z = sqrt(eps_x * (omega^2 - k_x.^2/eps_z));
else
    k_z = linspace(-k_max,k_max,N);
    k_x = sqrt(eps_z * (omega^2 - k_z.^2/eps_x));
end
%% plot
h = handles.axes1;
fsz = 12;
lw = 1;
plot(h,real(k_x),real(k_z),'LineWidth',lw);
xlabel('$k_x (2\pi/cm)$','Interpreter','Latex');
ylabel('$k_z (2\pi/cm)$','Interpreter','Latex');
set(gca,'FontSize',fsz,'FontName','Times New Roman');
end

    
